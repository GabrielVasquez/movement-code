int E1 = 9;       //Motor 1 and 2 Speed Pins
int E2 = 11;        
int E3 = 6;
int E4 = 5;

int M1 = 8;       //Motor 1 and 2 Direction Pins
int M2 = 12;        
int M3 = 7;
int M4 = 4; 

int S1 = 0;       //Motor 1 and 2 Speed variables
int S2 = 0;
int S3 = 0;
int s4 = 0;

bool D1 = 1;      //Motor 1 and 2 Direction variables
bool D2 = 2;

bool FORWARD = 1;
bool BACKWARD = 0;

#define sensor A0 // Sharp IR GP2Y0A41SK0F (4-30cm, analog)
#define sensor A1
#define sensor A2
#define sensor A3

float volts0 = 0;
float volts1 = 0;
float volts2 = 0;
float volts3 = 0;

int distance0 = 0;
int distance1 = 0;;
int distance2 = 0;
int distance3 = 0;


void drive (char a, bool dir1, bool dir2){       
  analogWrite(E1, a);
  digitalWrite(M1, dir1);
  analogWrite(E2, a);
  digitalWrite(M2, dir2);
  analogWrite(E3, a);
  digitalWrite(M3, dir2); 
  analogWrite(E4, a);
  digitalWrite(M4, dir1);
}


/*
void turn (char a, char b,char c, char d, bool dir){         
  analogWrite (E1, a);
  digitalWrite(M1, dir);
  analogWrite(E2, b);
  digitalWrite(M2, dir);
  analogWrite (E3, c);
  digitalWrite(M3, dir);
  analogWrite(E4, d);
  digitalWrite(M4, dir); 
}
 */
 
void setup() {
  for ( int i = 4; i <= 12; i++){
    pinMode(i, OUTPUT);
  }
  Serial.begin(9600);
  Serial.println("Run Keyboard control");
}

void loop() {

  volts0 = analogRead(sensor0)*0.0048828125;  // value from sensor * (5/1024)
  volts1 = analogRead(sensor1)*0.0048828125;  // value from sensor * (5/1024)
  volts2 = analogRead(sensor2)*0.0048828125;  // value from sensor * (5/1024)
  volts3 = analogRead(sensor3)*0.0048828125;  // value from sensor * (5/1024)
  
  distance0 = 13*pow(volts0, -1); // worked out from datasheet graph  Left Sensor
  distance1 = 13*pow(volts1, -1); // worked out from datasheet graph  Front Sensor
  distance2 = 13*pow(volts2, -1); // worked out from datasheet graph  Right Sensor
  distance3 = 13*pow(volts3, -1); // worked out from datasheet graph  Back Sensor
  
  delay(500); // slow down serial port 

  Serial.println(distance0);
  Serial.println(distance1);
  Serial.println(distance2);
  Serial.println(distance3);
  
  if(Serial.available()){
    byte val = Serial.read();
  byte firstBit = val / 100;      // ex 101 / 100 = 1 , 010 / 100 = 0
  byte secondBit = (val%100) / 10;  // ex 111 % 100 = 11 --> 11/10 = 1 , 101 % 100 = 1 --> 1/10 = 0 
  byte thirdBit = val % 1;      // ex 101 % 1 = 1 or 100 % 1= 0
  
  beginning(firstBit);
  middle(secondBit);
  end(thirdBit);
  
    }
  
  
}

void moveLeft(){
    //Strafe left
    S1 = 100;
    D1 = 0;
    D2 = 1;
    drive(S1, D2, D1);
}

void moveRight(){
  //Strafe right
    S1 = 100;
    D1 = 1;
    D2 = 0;
    drive(S1, D2, D1);
}

void moveFoward(){
  // move forward
  S2 = 100;
    D1 = 0;
    D2 = 0;
    drive(S2, D2, D1);
}

void moveBackward(){
  // move backward
  S2 = 100;
    D1 = 1;
    D2 = 1;
    drive(S2, D2, D1);
}

void stop(){
  // stop
  S2 = 0;
    drive(S2, D2, D1);
}

void accelerate(){
  if(S2 <= 200){
    S2 += 50;
    }
}

void decelerate(){
  if(S2 >= 50){
    S2 -= 50;
    }
}

void changeDirection(){
    D2 = !D2;
    drive(S2,D2, D1);
    break;
}




void beginning(byte firstBit){
  if(firstBit == 0){
    while(distance0 > 4){
      //Strafe left
      volts0 = analogRead(sensor0)*0.0048828125;
      volts2 = analogRead(sensor2)*0.0048828125;
      distance0 = 13*pow(volts0, -1);
      distance2 = 13*pow(volts2, -1);
      moveLeft();
      //S1 = 100;
      //D1=0;
      //D2 = 1;
      //drive(S1, D2, D1);
          }
          
          delay(250);
          
          while(distance0 <= distance2){
      //Strafe right
      volts0 = analogRead(sensor0)*0.0048828125;
      volts2 = analogRead(sensor2)*0.0048828125;
      distance0 = 13*pow(volts0, -1);
      distance2 = 13*pow(volts2, -1);
      moveRight();
            //S1 = 100;
            //D1=1;
            //D2 = 0;
            //drive(S1, D2, D1); 
        }
  }
  else{
    while(distance2 > 4){
      //Strafe right
      volts0 = analogRead(sensor0)*0.0048828125;
      volts2 = analogRead(sensor2)*0.0048828125;
      distance0 = 13*pow(volts0, -1);
      distance2 = 13*pow(volts2, -1);
      moveRight();
            //S1 = 100;
            //D1=1;
            //D2 = 0;
            //drive(S1, D2, D1); //Strafe right
          }
          
          delay(250);
          
          while(distance0 >= distance2){
      //Strafe left
      volts0 = analogRead(sensor0)*0.0048828125;
      volts2 = analogRead(sensor2)*0.0048828125;
      distance0 = 13*pow(volts0, -1);
      distance2 = 13*pow(volts2, -1);
      moveLeft();
            //S1 = 100;
            //D1=0;
            //D2 = 1;
            //drive(S1, D2, D1); 
        }
  }
  
  delay(250);
  
  //Full Speed Fwd
  moveFoward();
  
  delay(3000);
  
  //Stop
  stop();
  
  // Current location - in front of the ramp
}

void middle(byte secondBit){

  if(secondBit == 0){
  
    while(distance0 > 4){
      //Strafe left
      volts0 = analogRead(sensor0)*0.0048828125;
      volts2 = analogRead(sensor2)*0.0048828125;
      distance0 = 13*pow(volts0, -1);
      distance2 = 13*pow(volts2, -1);
      moveLeft();
      //S1 = 100;
      //D1=0;
      //D2 = 1;
      //drive(S1, D2, D1);
        }
          
    delay(250);
      
    while(distance1 > 4){
      volts1 = analogRead(sensor1)*0.0048828125;
      distance1 = 13*pow(volts1, -1);
      moveFoward();  
    } 
    
      stop();
    
    while(distance0 <= distance2){
      volts0 = analogRead(sensor0)*0.0048828125;
      volts2 = analogRead(sensor2)*0.0048828125;
      distance0 = 13*pow(volts0, -1);
      distance2 = 13*pow(volts2, -1);
      moveRight();      
    }
    
      stop();
  } 
  else{
  
    while(distance2 > 4){
      //Strafe right
      volts0 = analogRead(sensor0)*0.0048828125;
      volts2 = analogRead(sensor2)*0.0048828125;
      distance0 = 13*pow(volts0, -1);
      distance2 = 13*pow(volts2, -1);
      moveRight();
            //S1 = 100;
            //D1=1;
            //D2 = 0;
            //drive(S1, D2, D1); //Strafe right
          }
          
        while(distance1 > 4){
      volts1 = analogRead(sensor1)*0.0048828125;
      distance1 = 13*pow(volts1, -1);
      moveFoward();  
    } 
    
      stop(); 
      
    while(distance0 >= distance2){
      volts0 = analogRead(sensor0)*0.0048828125;
      volts2 = analogRead(sensor2)*0.0048828125;
      distance0 = 13*pow(volts0, -1);
      distance2 = 13*pow(volts2, -1);
      moveLeft();     
    }
    
      stop();
    
      } 
  }
  
void end(byte thirdBit){

  moveBackward();
  
  delay(1000);
  
  stop();
  
  //pick up that booty
  
  moveBackward();
  
  delay(5000);
  
  if(thirdBit == 0){
    moveLeft();
  }
  else{
    moveRight();
  }
  


}
