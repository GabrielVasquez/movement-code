int E1 = 9;       //Motor 1 and 2 Speed Pins
int E2 = 11;        
int E3 = 6;
int E4 = 5;

int M1 = 8;       //Motor 1 and 2 Direction Pins
int M2 = 12;        
int M3 = 7;
int M4 = 4; 

int S1 = 0;       //Motor 1 and 2 Speed variables
int S2 = 0;
int S3 = 0;
int s4 = 0;

bool D1 = 1;      //Motor 1 and 2 Direction variables
bool D2 = 2;

bool FORWARD = 1;
bool BACKWARD = 0;

void drive (char a, bool dir1, bool dir2){       
  analogWrite(E1, a);
  digitalWrite(M1, dir1);
  analogWrite(E2, a);
  digitalWrite(M2, dir2);
  analogWrite(E3, a);
  digitalWrite(M3, dir2); 
  analogWrite(E4, a);
  digitalWrite(M4, dir1);
}

void turn (char a, char b,char c, char d, bool dir){         
  analogWrite (E1, a);
  digitalWrite(M1, dir);
  analogWrite(E2, b);
  digitalWrite(M2, dir);
  analogWrite (E3, c);
  digitalWrite(M3, dir);
  analogWrite(E4, d);
  digitalWrite(M4, dir); 
}
 
void setup() {
  for ( int i = 4; i <= 12; i++){
    pinMode(i, OUTPUT);
  }
  Serial.begin(9600);
  Serial.println("Run Keyboard control");
}

void loop() {
  if(Serial.available()){
    char val = Serial.read();
    if(val != -1)
    {
      switch(val)
      {
      case 'w':          //Full Speed Fwd
        S2 = 100;
        D1=0;
        D2 = 0;
        drive(S2, D2, D1);
        break;
        
      case 's':         //Full Speed Bwd
        S2 = 100;
        D1= 1;
        D2 = 1;
        drive(S2, D2, D1);
        break;
        
      case 'x':         //Stop
        S2 = 0;
       drive(S2, D2, D1);
        break;

       case 'r':          //Strafe right
        S1 = 100;
        D1=1;
        D2 = 0;
        drive(S1, D2, D1);
        break;

       case 'l':          //Strafe left
        S1 = 100;
        D1=0;
        D2 = 1;
        drive(S1, D2, D1);
        break;
        
        case 'p':         //Speed up
        if(S2 <= 200){
          S2 += 50;
        }
   drive(S2, D2, D1);
        break;

        case 'o':         //Speed down
        if(S2 >= 50){
          S2 -= 50;
        }
      drive(S2, D2, D1);
        break;

        case 'n':         //change dir 
        
        D2 = !D2;
        drive(S2,D2, D1);
        break;
      }
    }
    else drive(0, 1, 0);  
  }
       
}
